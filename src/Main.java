import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {
        // OOP - stands for "Object Oriented Program".\
        // is a programming model that allows developers to design software around data or objects, rather than function and logic

        // OOP Concepts
            // Object - an abstract idea that represents something in th real world:
                // Example: The concept of a dog.
            // Class - representation of object using code. Blueprint/Framework
                // Example: writing a code that would describe a dog.
            // Instance - unique copy of the idea, made physical.
                // Example: Instantiating a dog named Browny from the dog class.

        // Objects
        // States and Attributes - what is the idea about. It would describe the properties that the object has.
        // Behaviours - what can an idea do?
            // Example: A person has attributes like name, age, height, and weight. A person can also eat, sleep, and speak.

        // Four Pillars of OOP
        // 1. Encapsulation - a mechanism of wrapping the data (variables) and code acting in the data (methods) together as a single unit.
            // "data handling" - the variables of a class will be hidden from other classes, and can be accessed only through the methods of the current class
                // variables/properties as private
                // provide a public setter and getter function

        // Instantiate a class Car with empty argument
        Car myCar = new Car("Vios", "Toyota", 2025);

        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getYearOfMake());

        // Setters
        myCar.setName("Civic Type R");
        System.out.println(myCar.getName());

        Car emptyCar = new Car();
        System.out.println(emptyCar.getName());


        // Composition and Inheritance
            // Both concept promotes code reuse through different approach.
        // Inheritance - it allows modelling an object that is a subset of another object.
        // Composition - allows modelling an object that are made up of other objects.

        System.out.println(myCar.getDriverName());

        Dog myPet = new Dog("Bantay", "White", "Askal");
        System.out.println(myPet.getName());
        System.out.println(myPet.getColor());

        myPet.setName("Mufasa");
        System.out.println(myPet.getName());

        // Abstraction - is a process where all the logic and complexity are hidden from the user

        // Interface - is used to achieve total abstraction
        // Creating abstract classes doesn't support multiple inheritance

        Person child = new Person();

        child.sleep();
        child.run();

        child.morningGreet();

        // Polymorphism
            // Greek word poly(many) and morph(forms)
            // In short, many forms

            // Two main types:
            // Static or compile time polymorphism - overloading
            // Dynamic or runtime polymorphism - overridden

        StaticPoly myAddition = new StaticPoly();

        myAddition.addition(1, 2);
        myAddition.addition(1.4, 2.7);
        myAddition.addition(1, 2,4);

        Parent myParent = new Parent();
        Child myChild = new Child();

        myParent.speak();
        myChild.speak();
    }
}