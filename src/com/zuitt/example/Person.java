package com.zuitt.example;

public class Person implements Actions, Greetings{
    public void sleep() {
        System.out.println("Zzzzzzzzz.....");
    }

    public void run() {
        System.out.println("Running........");
    }

    public void morningGreet() {
        System.out.println("Ohayo gozaimasu~~");
    }

    @Override
    public void afternoonGreet() {
        System.out.println("Guten tag");
    }
}
