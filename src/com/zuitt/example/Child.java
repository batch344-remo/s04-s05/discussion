package com.zuitt.example;

public class Child extends Parent{
    // The speak method from the Parent class will be overridden
    public void speak() {
        System.out.println("Hi, I am a child.");
    }
}
