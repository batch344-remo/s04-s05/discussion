package com.zuitt.example;


public class Car {
    // Access Modifiers - these are used to restrict the scope of a class, constructor, variable, method or data member.

    // Four Types of Access Modifiers:
        // 1. Default - no keyword indicated (Accessibility is within the package.)
        // 2. Private (private) - properties or methods are only accessible within the class.
        // 3. Protected - properties and methods that are only accessible by the class of the same package and the subclass present in any package.
        // 4. Public - properties and methods that can be accessed from anywhere.

    // Class Creation
        // 1. Properties - characteristics of an object
        private String name;
        private String brand;
        private int yearOfMake;

        // Make a component for a car through a composition
        private Driver driver;


        // 2. Constructor - used to create/instantiate an object. Keep in mind that the constructor will always be in public.
                // a. empty constructor - creates an object that does not have any arguments/parameters. Also referred to as default constructor.
                public Car() {
                    this.name = "Empty Car";
                    this.brand = "Empty Car";
                    this.yearOfMake = 0;
                }

                // b. parameterized - creates an object with arguments/parameters.
                public Car(String name, String brand, int yearOfMake) {
                        this.name = name;
                        this.brand = brand;
                        this.yearOfMake = yearOfMake;
                        this.driver = new Driver("Manong");
                }


        // 3. Getters and Setters - method that gets and sets the values of each property of an object. Keep in mind that the number of setters and getters depend on the number of properties.
                // Getters - it retrieves the value of instantiated objects.

                // Getter for the name property:
                public String getName() {
                        return this.name;
                }

                // Getter for the brand property
                public String getBrand() {
                        return this.brand;
                }

                // Getter for the yearOfMake Property
                public int getYearOfMake() {
                        return this.yearOfMake;
                }

                //Getter for the driver
                public String getDriverName() {
                    return this.driver.getName();
                }


                // Setters - use to change the default value of an instantiated object.

                // Setter for the name property
                public void setName(String name) {
                    this.name = name;
                }

                // Setter for the brand property
                public void  setBrand(String brand) {
                    this.brand = brand;
                }

                // Setter for the yearOfMake property
                public void setYearOfMake(int yearOfMake) {
                    this.yearOfMake = yearOfMake;
                }

                // Setter for driver
                public void setDriver(String driver) {
                    this.driver.setName(driver);
                }


        // 4. Methods - functions an object can perform (behaviour).
            public void drive() {
                    System.out.println("The car is running. Vroom. Vroom.");
            }
}
